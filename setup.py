from setuptools import setup, find_packages

setup(
    name='eryxdeploy',
    version='0.4.7',
    description='A tool to assist deployments for Eryx infraestructure',
    url='https://gitlab.com/eryx/eryxdeploy',
    author='Eryx Team',
    author_email='info@eryx.co',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'Fabric3==1.13.1.post1',
    ],
)
