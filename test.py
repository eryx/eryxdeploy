import unittest


class TestDeploymentsUsingFabric(unittest.TestCase):
    """
    Schema:
    - One docker container with a sample Django project which uses host's eryx-deploy tool throught Git.
    - Another docker container (compose) which acts as the remote host where the app should be deployed.
    """

    def setUp(self):
        # TODO
        # Create a Docker container which acts as a Developer Machine [1]. There, then:
        # - Create a sample Django project here.
        # - Configure project, install reqs (including this repo as a Git remote dependency [2]), etc. Includes creating
        # a fabfile.py
        # - Init a Git repo and do initial commit. Note we don't push anything here because we're not going to use a
        # hosted repo.
        #
        # [1] https://github.com/docker/docker-py
        # [2] https://docs.docker.com/engine/examples/running_ssh_service/
        #
        # Official django app with Docker guide: https://docs.docker.com/compose/django/
        pass

    def test_initial_deploy_is_successful(self):
        # TODO
        # 1. Create another Docker container. This simulates the remote server. This should use docker-compose to we
        # can test both MySQL and PostgreSQL cases.

        # 2. Perform initial deploy using eryxdeploy from the original docker container.
        #   Take into account that project should be cloned from the repo of the original docker container (we don't use
        #   a git server like gitlab when testing, so we don't have to create a real hosted repo.)
        # 3. Test initial deploy is sucessfull doing a request to localhost URL running on container.
        # 4. Use docker diff to check changes were performed: https://docs.docker.com/engine/reference/commandline/diff/
        # 5. Teardown the second docker container.
        pass

    def test_second_deploy_is_successfull_after_a_repo_change(self):
        # TODO
        # 1. Perform initial deployment
        # 2. Simulate a realistic change on the first container and commit on that repo.
        # 3. Run 'fab deploy' from there
        # 4. Use docker diff to check changes were performed: https://docs.docker.com/engine/reference/commandline/diff/
        pass

    def tearDown(self):
        # 1. Remove Docker "Developer machine" container
        pass


if __name__ == '__main__':
    unittest.main()
